package Merge;

public class MergeTeste {
	public static void Print(int[] Original) {
		for (int i = 0; i < Original.length; i++) {
			System.out.print(Original[i]+", ");
		}
	}

	public static void main(String[] args) {
		int [] Original= {8,4,1,5,33,27,11,51,42,13,17};
		System.out.println("Vetor Original!");
		Print(Original);
		
		Merge m= new Merge();
		m.MergeQuebra(Original);
		
		System.out.println("\n\nVetor Ordenado!");
		Print(Original);
		
		System.out.println("\n----------------------------------");
		
		int [] Original2= {8,4,1,5,33,27,11,51,42,13,17};
		
		System.out.println("\nVetor Original 2!");
		Print(Original2);
		
		Decrescente D = new Decrescente();
		D.MergeQuebra(Original2);
		System.out.println("\n\nVetor 2 Ordenado!");
		Print(Original2);
		
		
		
	}

}
