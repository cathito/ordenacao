package Quick;

import Merge.Merge;

public class TesteQuick {

	public static void Print(int[] Original) {
		for (int i = 0; i < Original.length; i++) {
			System.out.print(Original[i]+", ");
		}
	}
	
	public static void main(String[] args) {
		int [] Original= {8,4,1,5,33,27,11,7,16};
		System.out.println("Vetor Original!");
		Print(Original);
		
		Merge m= new Merge();
		m.MergeQuebra(Original);
		
		System.out.println("\n\nVetor Ordenado!");
		Print(Original);

	}

}
